using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace esh.core
{
	public class Core
	{
		public readonly List<Components.Sensor> Sensors;
		public readonly List<Components.Actor> Actors;
		public readonly List<Components.Trigger> Triggers;

		public Core()
		{
			Sensors = new List<Components.Sensor>();
			Actors = new List<Components.Actor>();
			Triggers = new List<Components.Trigger>();
		}

		public void UpdateSensorData(string mac, string dataType, string sensorType, string value, string displayUnit)
		{
			// We assume the mac address is network-unique, which they /should/ be
			Components.Sensor sensor;
			
			if (Sensors.Any(p => p.Mac.Equals(mac)))
				sensor = Sensors.First(p => p.Mac.Equals(mac));
			else
			{
				sensor = new Components.Sensor(mac);
				Sensors.Add(sensor);
			}

			sensor.DataType = dataType;
			sensor.SensorType = sensorType;
			sensor.Value = value;
			sensor.DisplayUnit = displayUnit;
			sensor.LastUpdated = DateTime.Now;
		}
		
		public void UpdateActorData(string mac, IPAddress ip, string dataType, string actorType, string state)
		{
			// We assume the mac address is network-unique, which they /should/ be
			Components.Actor actor;
			
			if (Actors.Any(p => p.Mac.Equals(mac)))
				actor = Actors.First(p => p.Mac.Equals(mac));
			else
			{
				actor = new Components.Actor(mac);
				Actors.Add(actor);
			}

			actor.WantsDataType = dataType;
			actor.ActorType = actorType;
			actor.LastKnownState = state;
			actor.LastPing = DateTime.Now;
			actor.LastKnownIP = ip;
		}
	}
}
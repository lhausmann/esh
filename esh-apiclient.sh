#!/bin/bash
# $1 = mac
# $2 = datatype
# $3 = sensortype
# $4 = value
# $5 = displayunit

curl -G http://localhost:5000/api --data-urlencode "mac=de:ad:be:ef:00:10" --data-urlencode "datatype=int" --data-urlencode "sensortype=Temperature" --data-urlencode "value=25" --data-urlencode "displayunit=°C" -v

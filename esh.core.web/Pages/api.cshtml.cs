using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace esh.core.web.Pages
{
	public class api : PageModel
	{
		public void OnGet()
		{

			bool isSensorQuery()
			{
				var sensorQuery = new List<string>()
				{
					"mac",
					"datatype",
					"sensortype",
					"value"
				};

				foreach (var item in sensorQuery)
				{
					if (!Request.Query.ContainsKey(item))
					{
						return false;
					}
				}

				return true;
			}
			
			bool isActorQuery()
			{
				var actorQuery = new List<string>()
				{
					"mac",
					"datatype",
					"actortype",
					"state"
				};

				foreach (var item in actorQuery)
				{
					if (!Request.Query.ContainsKey(item))
					{
						return false;
					}
				}

				return true;
			}
			
			if (isSensorQuery())
				Web.Core.UpdateSensorData(Request.Query["mac"], Request.Query["datatype"],
					Request.Query["sensortype"], Request.Query["value"], Request.Query["displayunit"]);
			else if (isActorQuery())
				Web.Core.UpdateActorData(Request.Query["mac"], Request.HttpContext.Connection.RemoteIpAddress,
					Request.Query["datatype"], Request.Query["actortype"], Request.Query["state"]);
			else
				Response.StatusCode = StatusCodes.Status400BadRequest;
		}
		
		public string GetActorValue(IQueryCollection requestQuery)
		{
			foreach (var trigger in Web.Core.Triggers)
			{
				if (trigger.condition.Type != Components.Trigger.Condition.ConditionType.Time &&
				    trigger.target.Mac == requestQuery["sensor"])
				{
					
				}
			}
			return Web.Core.Sensors.Any() ? Web.Core.Sensors.First().Value : "0";
		}
	}
}
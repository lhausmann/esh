using System;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace esh.core.web.Pages
{
	public class Config : PageModel
	{
		public void OnGet()
		{
			if (Request.Query.ContainsKey("action") && Request.Query["action"] == "modify")
			{
				if (Request.Query.ContainsKey("sensor") && Web.Core.Sensors.Any(p => p.Mac == Request.Query["sensor"]))
				{
					var sensor = Web.Core.Sensors.First(p => p.Mac == Request.Query["sensor"]);
					if (Request.Query.ContainsKey("name"))
						sensor.CustomName = Request.Query["name"];
					if (Request.Query.ContainsKey("description"))
						sensor.CustomDescription = Request.Query["description"];
					Response.Redirect("/Sensors");
				}
				else if (Request.Query.ContainsKey("actor") && Web.Core.Actors.Any(p => p.Mac == Request.Query["actor"]))
				{
					var actor = Web.Core.Actors.First(p => p.Mac == Request.Query["actor"]);
					if (Request.Query.ContainsKey("name"))
						actor.CustomName = Request.Query["name"];
					if (Request.Query.ContainsKey("description"))
						actor.CustomDescription = Request.Query["description"];
					Response.Redirect("/Actors");
				}
				return;
			}

			if (Request.Query.ContainsKey("action") && Request.Query["action"] == "newtrigger" &&
			    Request.Query.ContainsKey("name") && Request.Query.ContainsKey("conditionType") && 
			    Request.Query.ContainsKey("source") && Request.Query.ContainsKey("conditionValue"))
			{
				var condition = new Components.Trigger.Condition();
				condition.Type = Components.Trigger.Condition.ConditionType.ValueExact;
				var value = new Components.Value();
				value.Type = Components.Value.ValueType.Integer;
				value.StringValue = Request.Query["conditionValue"];
				condition.CheckValue = value;
				var action = new Components.Trigger.Action();
				action.Type = Components.Trigger.Action.ActionType.Passthrough;
				var trigger = new Components.Trigger(Request.Query["name"], condition, action);
				Web.Core.Triggers.Add(trigger);
				Response.Redirect("/Triggers");
				return;
			}

			if (Request.Query.ContainsKey("sensor") && Web.Core.Sensors.Any(p => p.Mac == Request.Query["sensor"]) ||
			    Request.Query.ContainsKey("actor") && Web.Core.Actors.Any(p => p.Mac == Request.Query["actor"]) ||
			    Request.Query.ContainsKey("trigger") && (Request.Query["trigger"] == "new" || 
			                                             Web.Core.Triggers.Any(p => p.id == Request.Query["trigger"])))
				return;
			Response.Redirect("/");
			Response.StatusCode = StatusCodes.Status400BadRequest;
		}
	}
}